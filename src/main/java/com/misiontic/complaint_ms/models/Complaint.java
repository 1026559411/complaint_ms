package com.misiontic.complaint_ms.models;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Complaint {

    @Id
    private String id;
    private String username;
    private String description;
    private Date date;

    public Complaint(String id, String username, String description, Date date) {
        this.id = id;
        this.username = username;
        this.description = description;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
