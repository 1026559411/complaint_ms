package com.misiontic.complaint_ms.controllers;

import com.misiontic.complaint_ms.models.Complaint;
import com.misiontic.complaint_ms.repositories.ComplaintRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ComplaintController {

    private final ComplaintRepository complaintRepository;

    public ComplaintController(ComplaintRepository complaintRepository) {
        this.complaintRepository = complaintRepository;
    }

    @GetMapping("/complaints/{id}")
    Complaint getComplaint(@PathVariable String id){

        return complaintRepository.findById(id).orElseThrow();
    }

    @GetMapping("/mycomplaints/{username}")
    List<Complaint> getComplaintsByUsername(@PathVariable String username){

        return complaintRepository.findByUsername(username);
    }

    @PostMapping("/complaints")
    Complaint newComplaint(@RequestBody Complaint complaint){
        return complaintRepository.save(complaint);
    }
}
