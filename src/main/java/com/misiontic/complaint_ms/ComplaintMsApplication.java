package com.misiontic.complaint_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComplaintMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComplaintMsApplication.class, args);
	}

}
