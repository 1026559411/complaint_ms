package com.misiontic.complaint_ms.repositories;

import com.misiontic.complaint_ms.models.Complaint;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ComplaintRepository extends MongoRepository<Complaint, String> {

    List<Complaint> findByUsername (String username);
}
